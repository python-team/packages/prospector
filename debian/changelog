prospector (1.10.3-1) unstable; urgency=medium

  * Team Upload
  * Fix watchfile for new Github API
  * New upstream version 1.10.3 (Closes: #947551, #963509, #1071808)
  * Pylint support plugin is temporarily patched-out.
  * Update build-dependencies:
    + dh-sequence-python3
    + pybuild-plugin-pyproject
    + python3-bandit
    - python3-mock
    - python3-nose (Closes: #1018441)
    + python3-poetry-core
    + python3-pytest
    + python3-pyrome
    + python3-vulture
  * Remove dependency on python3-typed-ast (Closes: #1055672)

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Mon, 05 Aug 2024 22:41:53 +0200

prospector (1.1.7-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on dodgy, pydocstyle,
      python3-mccabe, python3-pyflakes and python3-requirements-detector.
    + prospector: Drop versioned constraint on dodgy in Depends.
    + prospector: Drop versioned constraint on vulture in Recommends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 13 Dec 2022 19:18:09 +0000

prospector (1.1.7-3) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * debian/patches/72777c6f96d7c393abf9cc44b423ff0f54029feb.patch
    - Fix test since mixed-indentation does not exists anymore

 -- Sandro Tosi <morph@debian.org>  Fri, 01 Apr 2022 15:05:07 -0400

prospector (1.1.7-2) unstable; urgency=medium

  * Team upload
  * tighteen pylint dependency
    (Closes: #946553)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 15 Dec 2019 11:20:42 +0100

prospector (1.1.7-1) unstable; urgency=medium

  [ ChangZhuo Chen (陳昌倬) ]
  * New upstream release. (Closes: #919793, #896624)
  * Update Vcs-* fields to salsa.
  * Remove upstream already applied patch.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Gianfranco Costamagna ]
  * Team upload.
  * Set R^3 to no
  * Import new upstream release 1.1.7 (Closes: #942408)
  * Bump compat level to 12
  * Bump std-version to 4.4.1
  * Import 0.12.7-2.1 NMU
  * Switch to new pylint, the old pylint3 is now a cruft binary
  * Update watch file for new location
  * Update dependencies according to new version

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Dec 2019 09:00:35 +0100

prospector (0.12.7-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Run dh_python3 with --shebang=/usr/bin/python3. Closes: #918427.

 -- Matthias Klose <doko@debian.org>  Tue, 12 Mar 2019 16:31:35 +0100

prospector (0.12.7-2) unstable; urgency=medium

  * Team upload.
  * No-change rebuild for the python 3.6 transition.  Closes: #882789

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 03 Dec 2017 17:44:21 +0100

prospector (0.12.7-1) unstable; urgency=medium

  * New upstream release.
  * Merge to unstable.
  * Bump Standards-Version to 4.0.0.
  * Update Build-Depends.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 18 Jun 2017 20:14:07 +0800

prospector (0.12.6-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update copyright.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 08 Jun 2017 05:10:19 +0800

prospector (0.12.4-3~exp1) experimental; urgency=medium

  * Change Uploaders to ChangZhuo Chen (Closes: #863363)
  * Bump compat to 10.
  * Update Vcs-* fields to use Git as VCS.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 28 May 2017 17:49:39 +0800

prospector (0.12.4-2) unstable; urgency=medium

  * add unpin-pycodestyle.patch (Closes: #852048).

 -- Daniel Stender <stender@debian.org>  Tue, 07 Feb 2017 07:50:05 +0100

prospector (0.12.4-1) unstable; urgency=medium

  * New upstream release.
  * deb/control: change Section to "devel" (application-in-library-section).

 -- Daniel Stender <stender@debian.org>  Fri, 02 Dec 2016 18:27:18 +0100

prospector (0.12.2-1) unstable; urgency=medium

  * New upstream release (Closes: #833659).
  * change over to pycodestyle (from pep8):
    + depend on python3-pycodestyle in deb/control.
    + update extended description in deb/control.
    + update manpage (deb/prospector.txt).
    + update deps for PEP-8 tests in deb/tests/control.
  * deb/control:
    + add version to python3-mccabe build dep.
  * deb/prospector.txt:
    + use more line breaks to make the source easier to handle.
    + add info on new vscode output format in OPTIONS.
    + some cosmetics in info block on supported tools.

 -- Daniel Stender <stender@debian.org>  Sat, 13 Aug 2016 21:37:02 +0200

prospector (0.12-1) unstable; urgency=medium

  * New upstream release.
  * deb/control:
    + build depend on pydocstyle instead of pep257.
    + updated versioned build and package deps.
    + updated description of binary package, dropped dep against
      pep257.
  * deb/copyright: expanded copyright span.
  * deb/tests/control: updated.
  * manpage: updated, removed obsolete stuff, cosmectics.
  * Dropped sort-for-eggs.patch (applied upstream).
  * Added deb/NEWS (with info on deprecated pep257).

 -- Daniel Stender <stender@debian.org>  Fri, 24 Jun 2016 22:53:55 +0200

prospector (0.11.7-7) unstable; urgency=medium

  * deb/control: bumped standards to 3.9.8 (no changes needed).
  * deb/rules: dropped LC_ALL=C.UTF-8 in dh_auto_install override.
  * Added sort-for-eggs.patch, deb/source/options.

 -- Daniel Stender <stender@debian.org>  Sun, 29 May 2016 13:02:31 +0200

prospector (0.11.7-6) unstable; urgency=medium

  * deb/rules: set LC_ALL for custom auto_install to support reproducible
    builds (different locales alter the sorting order of egg-info/requires.txt).

 -- Daniel Stender <stender@debian.org>  Fri, 01 Apr 2016 22:01:56 +0200

prospector (0.11.7-5) unstable; urgency=medium

  * Upload to unstable.
  * deb/control: added versioned binary package deps for needed packages
    build on Python3 (pep257 and dodgy).

 -- Daniel Stender <stender@debian.org>  Thu, 24 Mar 2016 12:53:22 +0100

prospector (0.11.7-4) experimental; urgency=medium

  * Build/run on Python 3:
    + changed deps in deb/control (and sorted logically).
    + build with dh_python3, install with python3 in deb/rules.
    + updated deps in deb/tests/control.
    + test against python3 in deb/tests/prospector.
  * deb/rules: use Pybuild custom args also for dh_auto_install.

 -- Daniel Stender <stender@debian.org>  Wed, 23 Mar 2016 09:53:16 +0100

prospector (0.11.7-3) unstable; urgency=medium

  * deb/tests/control: updated pep8 dependency.

 -- Daniel Stender <stender@debian.org>  Sun, 06 Mar 2016 12:44:27 +0100

prospector (0.11.7-2) unstable; urgency=medium

  * Updated maintainer email address.
  * deb/control:
    + updated pep8 dependency.
    + bumped Standards-Version to 3.9.7 (no changes needed).

 -- Daniel Stender <stender@debian.org>  Fri, 04 Mar 2016 12:49:45 +0100

prospector (0.11.7-1) unstable; urgency=medium

  * New upstream release (Closes: #807305).
  * deb/control:
    + updated needed versions of build-deps.
    + add sphinx-argparse and pylint-flask to build-deps.
    + set Vcs-Browser URL to HTTPS.
    + dropped Testsuite field (deprecated).
    + added vulture and python-pyroma to Recommends.
    + updated description on supported tools.
  * deb/copyright: expanded copyright span.
  * deb/rules:
    + dropped nodocs option in dh_installdocs override (obsolete).
    + generate README from README.rst (changed from Markdown).
  * manpage: added info on how to run with the additional tools and
    on support of pylint-flask.
  * Dropped:
    + docs-no-sphinx-argparse.patch (sphinx-argparse available now).
    + docs-correct-some-flaws.patch (applied upstream).
  * Updated remaining patches.

 -- Daniel Stender <debian@danielstender.com>  Sun, 31 Jan 2016 18:18:25 +0100

prospector (0.10.1+git20150706.a00e191-1) unstable; urgency=medium

  * Initial release (Closes: #781165).

 -- Daniel Stender <debian@danielstender.com>  Wed, 19 Aug 2015 13:32:00 +0200
